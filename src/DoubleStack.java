import java.util.LinkedList;

public class DoubleStack {


    private LinkedList<Double> stack;


    public static void main(String[] argum) {
        String s1 = "1.5 2.5 +";
        double res1 = interpret(s1);
        System.out.println(res1);
    }

    DoubleStack() {
        stack = new LinkedList<Double>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        //https://www.tutorialspoint.com/java/util/linkedlist_clone.html
        // Copy of the stack: Object clone()
        DoubleStack cloned = new DoubleStack();
        cloned.stack = (LinkedList<Double>) stack.clone();
        return cloned;
    }

    public boolean stEmpty() {
        return (stack.isEmpty()) ? true : false;
    }

    public void push(double a) {
        stack.push(a);
    }

    public double pop() {
        //Removing an element from the stack: long pop()
        if (!stEmpty()) {
            return stack.pop();
        }
        throw new RuntimeException("Cannot pop from mpty stack");
    }

    public void op(String s) {
        // Arithmetic operation s ( + - * / ) between two topmost elements of the stack (result is left on top):
        // https://stackoverflow.com/questions/26969080/convert-string-to-operator-in-java

        if (!stEmpty()) {

            char op = s.charAt(0);
            double num2 = stack.pop();
            double num1 = stack.pop();

            switch (op) {
                case '+':
                    stack.push(num1 + num2);
                    break;

                case '-':
                    stack.push(num1 - num2);
                    break;

                case '*':
                    stack.push(num1 * num2);
                    break;

                case '/':
                    stack.push(num1 / num2);
                    break;

                default:
                    throw new RuntimeException("Operation ' " + op + " ' is invalis, valid operations are +, -, * and /)");
            }
        }
    }

    public double tos() {
        //Reading the top without removing it:
        if (!stEmpty()) {
            return stack.peek();
        }
        throw new RuntimeException("Stack cannot be empty!");
    }

    @Override
    public boolean equals(Object o) {
        //Check whether two stacks are equal: boolean equals
        //https://coderanch.com/t/525745/java/Convert-LinkedList-Object-LinkedList
        if (o instanceof DoubleStack) {
            //"cast" object to LinkedList
            LinkedList<Double> objList = ((DoubleStack) o).stack;
            return (stack.equals(objList)) ? true : false;
        }
        return false;
    }

    @Override
    public String toString() {
        //Conversion of the stack to string (top to last)
        StringBuilder sb = new StringBuilder();

        for (int i = stack.size() - 1; i >= 0; i--) { // such numeration to make top last (iterate from end)
            sb.append(stack.get(i).toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    public static double interpret(String pol) {
        // calculate the value of an arithmetic expression pol in RPN (Reverse Polish Notation) using this stack type
        DoubleStack ds = new DoubleStack();
        //counters for numbers and operands:
        int nums = 0;
        int ops = 0;
        // if null or empty string is sent:
        if (pol == null || pol.length() == 0) {
            throw new RuntimeException("Empty input!");
        }

        //removing tabs and spaces
        pol = pol.replaceAll("\\s+", " ");
        pol = pol.trim();
        String[] arr = pol.split("\\s+");
        String operands = "+-*/"; //available math operands

        for (String s : arr) {
            try {
                //while receiving numbers - push them to stack:
                ds.push(Double.parseDouble(s));
                nums++;
            } catch (NumberFormatException nfe) {
                //check if now we receiving operands +-*/
                if (operands.contains(s.trim())) {
                    ops++;
                    //check if operand inserted first
                    if (nums == 0)
                        throw new RuntimeException("Invalid input: " + pol + ", operand cannot be first!");
                    if (nums == ops) throw new RuntimeException("Invalid input: " + pol);

                    //all OK, can make operation

                    ds.op(s);
                } else {
                    throw new RuntimeException("Invalid operand " + s + " in input " + pol);
                }
            }
        }

        //final check, nums-1 should always be equal to ops:
        if (nums - 1 != ops) {
            throw new RuntimeException("Wrong stackbalance! Input " + pol + " is illegal!");
        }

        return ds.pop();
    }

}

